<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
set_time_limit(0);


$apikey       = 'aW8djmz0DZABldP7RFTq';
$apiurl       = 'https://api.yourtravis.com';
$apiv         = '2.0';
$apiurl       = $apiurl.'?v='.$apiv.'&key='.$apikey;

function docurl( $apiurl, $params, $debug = false ) {

  $params       = http_build_query( $params );
  $requesturl   = $apiurl.'&'.$params;

  if( $debug === true ) {
    echo 'REQUEST: '.$requesturl.'<hr>';
  }

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $requesturl);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  $response = curl_exec($ch);

  if (!curl_errno($ch)) {
    $info = curl_getinfo($ch);

    $responsejson = json_decode( $response );
    if( $debug === true ) {
      echo 'RESPONSE: <br/>';
      echo '<pre>';
      var_dump( $responsejson );
      echo '</pre><hr>';
    }
    return $responsejson;
  } else {
    return false;
  }
  curl_close($ch);


}

function obj_to_array($obj, $arr = [], $prefix = '') {
  foreach( $obj as $k => $v ) {
    $key = join( '-', array_filter( [$prefix, $k] ) );
    if( is_object( $v ) ) {
      $arr = obj_to_array( $v, $arr, $key );
    } else {
      $arr[$key] = $v;
    }
  }

  return $arr;
}
?>
