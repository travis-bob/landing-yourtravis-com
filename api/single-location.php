<?php

$id = filter_var( $_GET['id'], FILTER_VALIDATE_INT ) ? $_GET['id'] : die('NO NUMBER');
$debug = isset($_GET['debug']) ? true : false;
$clear = isset($_GET['clear']) ? true : false;

$starttime = microtime(true);
$message;


function debug($label, $output = null) {
  global $debug, $starttime, $message;
  if ($debug) {
    $now = microtime(true);
    $elapsed = $now - $starttime;
    $elapsed = round ($elapsed, 5);
    $message .= $elapsed . ' sec. - ' . $label . '<br /><br />';
    if ($output != null) {
      $message .= $output . '<br /><br />';
    }
  }
}


debug ('START');

define( 'WP_USE_THEMES', false );
require( '../../../../wp-load.php' );
include 'travis.php';


$params       = array(
  'component'    => 'locations',
  'action'       => 'getlist',
  'locationid'   => $id,
);


debug('GET RESPONSE');

$response = docurl( $apiurl, $params, false );

foreach( $response as $loc ) {

  debug ('RESPONSE ', json_encode($loc));

  $metafields = obj_to_array( $loc );

  debug ('METAFIELDS ', json_encode($metafields));

  if ($clear) {

    $clearmetafields = array (
      'parking-openinghours-weekend',
      'parking-openinghours-friday',
      'parking-openinghours-saturday',
      'parking-openinghours-sunday',
    );

    foreach ($clearmetafields as $clear) {
      $metafields[$clear] = isset ($metafields[$clear]) ? $metafields[$clear] : '';
      // echo $clear . ' ' . $metafields[$clear] . '<br />';
    }
    debug('METAFIELDS CLEARED', json_encode($metafields));
  }
  else {
    debug('NO METAFIELDS CLEARED (add &clear to clear metafields)');
  }


  if( $metafields['id'] == $id ) {

      $query = $wpdb->prepare( 'SELECT post_id FROM '.$wpdb->postmeta.' WHERE meta_key = "id" AND meta_value = %s', $metafields['id'] );
      $wpdb->query( $query );

      $post_id = $wpdb->get_var( $query );


      // check if location does exist. If not, create
      if ($post_id == '') {
        $args = array(
         'post_title'    => $metafields['name'],
         'post_status'   => 'publish',
         'post_date'     => date('Y-m-d H:i:s'),
         'post_author'   => 2,
         'post_type'     => 'location',
         'post_name'     => $metafields['url_part'],
        );
        $post_id = wp_insert_post( $args );
        debug('CREATED POST ' . $post_id);
      } else {
        wp_update_post( array( 'ID' => $post_id, 'post_title' => $metafields['name'], 'post_name' => $metafields['url_part'] ) );
      }

      $services         = [];
      if( $metafields['services-truckwash'] ) {
        $services[]     = 'Truckwash';
      }
      if( $metafields['services-tankcleaning'] ) {
        $services[]     = 'Tankcleaning';
      }
      if( $metafields['services-repair'] ) {
        $services[]     = 'Repair';
      }
      if( $metafields['services-carwash'] ) {
        $services[]     = 'Carwash';
      }
      if( $metafields['services-parking'] ) {
        $services[]     = 'Parking';
      }

      wp_set_object_terms( $post_id, $metafields['address-country'], 'location_country', true );
      wp_set_object_terms( $post_id, $metafields['address-city'], 'location_city', true );
      wp_set_object_terms( $post_id, $services, 'location_service', true );


      // if truckwash, then update pricelist
      if ($metafields['services-truckwash']) {
        $pricelistparams = array(
          'component'    => 'locations',
          'action'       => 'getpricelist',
          'locationid'   => $id
        );

        $pricelistresponse = docurl( $apiurl, $pricelistparams, false );

        $pricelistv2 = '';
        if( $pricelistresponse ) {

          foreach( $pricelistresponse as $pricelistloc ) {
            if( !empty( $pricelistloc->program_id ) ) {
              $pricelistv2 .= $pricelistloc->program_id.'~'.$pricelistloc->price."\n";
            }
          }

          $metafields['truckwash-pricelistv2'] = $pricelistv2;
          debug('UPDATED PRICELIST', $pricelistv2);

        }
      }


      foreach( $metafields as $key => $value ) {
        update_post_meta( $post_id, $key, $value );
      }

      debug('UPDATED ID ' . $id . ' - ' .  $metafields['name']);

      $path = '/location/' . $metafields['url_part'] . '/';
      //strl_flush_cache ($path);

      debug('CACHE FLUSHED ' . $path);



  }
}


// do_action( 'savvii_cache_flush' );
// do_action( 'savvii_domain_flush' );

debug('DONE');

if ($debug) {
  echo $message;
}
else {
  $elapsed = microtime(true) - $starttime;
  echo 'SUCCESS in ' . $elapsed;
}
?>
