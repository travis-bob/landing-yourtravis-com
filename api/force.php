<?php

/* todo
fix include (with output)


*/

$blog = filter_var( $_GET['blogid'], FILTER_VALIDATE_INT ) ? $_GET['blogid'] : die('NO NUMBER');
$force = isset($_GET['force']) ? true : false ;

$starttime = microtime(true);

define( 'WP_USE_THEMES', false );
require( '../../../../wp-load.php' );

include 'travis.php';

$params       = array(
  'component'    => 'locations',
  'action'       => 'getlist',
);

$response = docurl( $apiurl, $params, false );

if ( is_multisite() ) {
  switch_to_blog( $blog );
}

//print_r($response);
if ($force) {

  $args = array(
    'post_type'         => 'location',
    'posts_per_page'    => -1
  );

  $the_query = new WP_Query( $args );

  $post_ids = wp_list_pluck( $the_query->posts, 'ID' );
  foreach ($post_ids as $key => $value) {
    echo $value . '<br />';
    update_post_meta( $value, 'dates-last_update', '1' );
  }
  //print_r($post_ids);


  $time_elapsed_secs = microtime(true) - $starttime;
  exit ('DONE in ' . $time_elapsed_secs . ' seconds');

}


//$blogpath = strl_get_path_from_blogid ($blog);

//echo 'BLOGID: ' . $blog . ' - ' . $blogpath . '<br /><br />';

$total_services = 0;

if( $response && !isset( $response->status )  ) {
  foreach( $response as $loc ) {

    $metafields = obj_to_array( $loc );

    $query = $wpdb->prepare( 'SELECT post_id FROM '.$wpdb->postmeta.' WHERE meta_key = "id" AND meta_value = %s', $metafields['id'] );
    $wpdb->query( $query );

    if( $wpdb->num_rows ) {
      $post_id = $wpdb->get_var( $query );
      $travis_last_update = $metafields['dates-last_update'];
      $wp_last_update = get_post_meta( $post_id, 'dates-last_update', true );

      if ( $travis_last_update > $wp_last_update || isset( $force ) ) {
        echo 'TRAVIS-last_update: ' . $travis_last_update . '<br />';
        echo 'WP-last_update: ' . $wp_last_update . '<br />';
        echo 'UPDATED '.$post_id .' | '.$metafields['id'].' | '.$metafields['name'].'<br/><br/>';
        //include('./single-location.php?id=' .$metafields['id']);
      }

      else {
        echo 'SKIP '.$post_id .' | '.$metafields['id'].' | '.$metafields['name'].'<br/><br/>';
      }


      $foundposts[] = $post_id;
      //die;
    } else {
      echo 'INSERT | '.$metafields['name'].'<br/>';

      $args = array(
       'post_title'    => $metafields['name'],
       'post_status'   => 'publish',
       'post_date'     => date('Y-m-d H:i:s'),
       'post_author'   => 2,
       'post_type'     => 'location',
       'post_name'     => $metafields['url_part'],
      );
      $post_id = wp_insert_post( $args );
      update_post_meta( $post_id, 'id', $metafields['id'] );
      //include('./single-location.php?id=' .$metafields['id']);
      $foundposts[] = $post_id;
    }

    if( $metafields['services-truckwash'] ) {
      $services[]     = 'Truckwash';
      $total_services++;
    }
    if( $metafields['services-tankcleaning'] ) {
      $services[]     = 'Tankcleaning';
      $total_services++;
    }
    if( $metafields['services-repair'] ) {
      $services[]     = 'Repair';
      $total_services++;
    }
    if( $metafields['services-carwash'] ) {
      $services[]     = 'Carwash';
      $total_services++;
    }
    if( $metafields['services-parking'] ) {
      $services[]     = 'Parking';
      $total_services++;
    }

    $currentposts = get_posts('post_type=location&posts_per_page=-1');
    foreach( $currentposts as $post ) {
      if( !in_array( $post->ID, $foundposts ) ) {
        //echo 'DELETE: '.$post->ID.'<br/>';
        //wp_delete_post( $post->ID );
      }
    }

  }
} else {
  echo 'Invalid response from API.<br/>';
}

update_field('shortcode_total_services', $total_services, 'options');
echo 'SERVICES: ' . $total_services . '<br/><br/>';

$time_elapsed_secs = microtime(true) - $starttime;
exit('DONE in ' . $time_elapsed_secs . ' seconds');
?>
