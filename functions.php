<?php
/**
 * Theme functions and definitions
 *
 * @package HelloElementorChild
 */

/**
 * Load child theme css and optional scripts
 *
 * @return void
 */
function hello_elementor_child_enqueue_scripts() {
	wp_enqueue_style(
		'hello-elementor-child-style',
		get_stylesheet_directory_uri() . '/style.css',
		[
			'hello-elementor-theme-style',
		],
		'1.0.0'
	);
}
add_action( 'wp_enqueue_scripts', 'hello_elementor_child_enqueue_scripts', 20 );


// Function to load blog ID path
function strl_get_path_from_blogid ( $blogid, $noslash = false ) {
	$path = '';
	if ( is_multisite() ) {
  	$path = get_blog_details( $blogid )->path;
	}
  if ($noslash == true) {
    $path = str_replace('/', '', $path);
  }
  return $path;
}


// Add post type locations
if ( !post_type_exists('location') ) {
	function register_location_type() {
		$label_singular = 'Location';
		$label_plural   = 'Locations';

		register_post_type(
			'location',
			array(
				'label'           => $label_plural,
				'public'                  => true,
				'show_in_rest' 						=> true,
				'has_archive'             => false,
				'rewrite'                 => array( 'slug' => 'location' ),
				'menu_position'           => 5,
				'menu_icon'               => 'dashicons-welcome-widgets-menus',
				'supports'                => array( 'title','thumbnail','author','revisions' ),
				'labels' => array (
					'name'               => $label_plural,
					'singular_name'      => $label_singular,
					'menu_name'          => $label_plural,
					'add_new'            => 'Add New',
					'add_new_item'       => 'Add New ' . $label_singular,
					'edit'               => 'Edit',
					'edit_item'          => 'Edit ' . $label_singular,
					'new_item'           => 'New ' . $label_singular,
					'view'               => 'View ' . $label_singular,
					'view_item'          => 'View ' . $label_singular,
					'search_items'       => 'Search ' . $label_plural,
					'not_found'          => 'No ' . $label_plural . ' Found',
					'not_found_in_trash' => 'No ' . $label_plural . ' Found in Trash',
					'parent'             => 'Parent ' . $label_singular,
				)
			)
		);
	}
	add_action('init', 'register_location_type');
}


add_action( 'init', 'create_taxonomies' );

function create_taxonomies(){

	register_taxonomy(
		'location_country',
		'location',
		array(
			'label'        => __( 'Country', 'strl' ),
			'rewrite'      => array( 'slug' => 'country' ),
			'hierarchical' => true,
      'show_admin_column'  => true
		)
	);

  register_taxonomy(
    'location_city',
    array('location', 'page'),
    array(
      'label'        => __( 'City', 'strl' ),
      'rewrite'      => array( 'slug' => 'city' ),
      'hierarchical' => true,
      'show_admin_column'  => true
    )
  );

  register_taxonomy(
    'location_service',
    'location',
    array(
      'label'        => __( 'Service', 'strl' ),
      'rewrite'      => array( 'slug' => 'service' ),
      'hierarchical' => true,
      'show_admin_column'  => true
    )
  );

  register_taxonomy(
    'news_type',
    'news',
    array(
      'label'        => __( 'Types', 'strl' ),
      'rewrite'      => array( 'slug' => 'type' ),
      'hierarchical' => true,
      'show_admin_column'  => true
    )
  );
}


// Delete instead of Trash
function directory_skip_trash($post_id) {
    if (get_post_type($post_id) == 'location') {
        // Force delete
        wp_delete_post( $post_id, true );
    }
}
add_action('trashed_post', 'directory_skip_trash');


// Add ACF json path
add_filter('acf/settings/save_json', 'strl_acf_json_location');
add_filter('acf/settings/load_json', 'strl_acf_json_location');
function strl_acf_json_location( $path ) {
	$path = get_stylesheet_directory().'/acf-json/';
	return $path;
}


// Add filter possibility in json
// https://landing.yourtravis.com/wp-json/wp/v2/location/?partnerid=1434
// https://landing.yourtravis.com/wp-json/acf/v3/location/?partnerid=1434
// https://wordpress.org/support/topic/cant-update-acfs-using-json/
// https://github.com/airesvsg/acf-to-rest-api/issues/13
// https://deliciousbrains.com/wordpress-rest-api-vs-custom-request-handlers/
add_filter( 'rest_query_vars', function ( $valid_vars ) {
    return array_merge( $valid_vars, array( 'partnerid', 'meta_query' ) );
} );

add_filter( 'rest_location_query', function( $args, $request ) {
    $partnerid   = $request->get_param( 'partnerid' );

    if ( ! empty( $partnerid ) ) {
        $args['meta_query'] = array(
            array(
                'key'     => 'id',
                'value'   => $partnerid,
                'compare' => '=',
            )
        );
    }

    return $args;
}, 10, 2 );


// increase per_page maximum to 1000
add_filter("rest_location_collection_params", function($params) {
    $params['per_page']['maximum'] = 5000;
    return $params;
});


// Flush Cache
function strl_flush_cache( $path, $status = null ) { // $path, $post_id, $blog_id, $status = null

  // WP Rocket clear post cache
  rocket_clean_post( $path );

  // SAVVII cache clear
  $headers = [];
  $curl = curl_init('yourtravis.com');

  # Specify host and path to flush.
  $host = 'yourtravis.com';
  //$path = '/' . $slug . '/location/' . $url_part . '/';
  //echo 'path: ' . $path . '<br />';

  $headers[] = "X-Purge-Host: $host";
  $headers[] = "X-Purge-Path: $path";

  curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PURGE");
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_HEADER, true);
  curl_setopt($curl, CURLOPT_NOBODY, true);
  curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

  $response = curl_exec($curl);
  $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
  curl_close($curl);

  if(!$response) {
    return 'savvii: error: no response';
  }

  if($status === null) {
    if($httpCode < 400) {
      return 'savvii http: success ' . $httpCode . ' - ' . $path;
    } else {
      return 'savvii: error ' . $httpCode;
    }
  }

  elseif($status == $httpCode) {
    return 'savvii status: success ' . $httpCode . ' - ' . $path;
  }

  else {
    return 'savvii: error ' . $httpCode;
  }

}
